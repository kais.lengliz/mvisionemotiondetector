import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:emotiondetector/modelandservice/Complaint.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoriquePage extends StatefulWidget {


  HistoriquePage() {}

  @override
  _HistoriquePageState createState() => _HistoriquePageState();
}

class _HistoriquePageState extends State<HistoriquePage> {
  static const double _cardElevation = 9;
  static const double _cardBorderRadius = 11;
  final List<Complaint> _allEmotions = [];

  @override
  @override
  void initState() {
    super.initState();
    getprefts();

  }

  getprefts() async{
    final prefs = await SharedPreferences.getInstance();


    Set<String> p = prefs.getKeys();
    p.forEach((element) {


      Complaint c = Complaint.fromJson(jsonDecode(prefs.get(element)));
      setState(() {
        _allEmotions.add(c);
      });



    });
    print(jsonEncode(_allEmotions));
    return json.decode(json.encode(prefs.get("1")));
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

        body: new Padding(
            padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
            child: getHomePageBody(context)));
  }

  getHomePageBody(BuildContext context) {
    return ListView.builder(
      itemCount: _allEmotions.length,
      itemBuilder: _getItemUI,
      padding: EdgeInsets.all(0.0),
    );
  }

  Widget _getItemUI(BuildContext context, int index) {

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
      ),
      clipBehavior: Clip.antiAlias,
      elevation: _cardElevation,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _Image(_allEmotions[index].pathimg),
          _Content(_allEmotions[index]),
        ],
      ),
    );
  }
}
class _Image extends StatelessWidget {
static const double _imageHeight = 250/851;
const _Image(this._url);

final String _url;

@override
Widget build(BuildContext context) {
  var screenHeight = MediaQuery.of(context).size.height;
  return Image.file(
      File (_url),
      fit: BoxFit.cover,
      height: _imageHeight*screenHeight,
  );
}
}

///Displays [Wisdom.text], [Wisdom.type], [Wisdom.id] and
///a [_LikeButton]
class _Content extends StatelessWidget {
  static const double _smallPadding = 4;
  static const double _largePadding = 8;
  final Complaint _complaint;

  const _Content(this._complaint);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: _largePadding, bottom: _largePadding),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 5,
              child: ListTile(
                title: Text(_complaint.Emotion),
                /*subtitle: Container(
                    padding: EdgeInsets.only(top: _smallPadding),
                    child: Text(_wisdom.type + ' #' + '${_wisdom.id}',
                        textAlign: TextAlign.left)),*/
              )),
          Expanded(
              flex: 5,
              child: ListTile(
                title: Text( "date :" + _complaint.timestampsend),
                /*subtitle: Container(
                    padding: EdgeInsets.only(top: _smallPadding),
                    child: Text(_wisdom.type + ' #' + '${_wisdom.id}',
                        textAlign: TextAlign.left)),*/
              )),


        ],
      ),
    );
  }

}


///Figures out if a Wisdom is already liked or not.
///Then send corresponding Event.


