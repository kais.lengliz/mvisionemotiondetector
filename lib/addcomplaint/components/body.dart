import 'dart:convert';
import 'dart:io';

import 'package:emotiondetector/modelandservice/rounded_button.dart';
import 'package:export_video_frame/export_video_frame.dart';
import 'package:intl/intl.dart';
import 'package:emotiondetector/modelandservice/Complaint.dart';
import 'package:emotiondetector/modelandservice/complaintservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:sweetalert/sweetalert.dart';
import 'package:video_player/video_player.dart';

import 'background.dart';


class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {


  static final DateTime now = DateTime.now();
  static final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(now);
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool _saving = false;
  File _image;
  File file;

  var _isClean = false;

  Future _getImages() async {
    _saving = true;
    setState(() {

    });
    this.file = await ImagePicker.pickVideo(source: ImageSource.gallery);
    var images = await ExportVideoFrame.exportImage(file.path, 10, 0);
    var result = images.map((file) => Image.file(file)).toList();
    setState(() {
      _saving = false;
      _controller = VideoPlayerController.network(
          file.path
      );
      // Initialize the controller and store the Future for later use.
      _initializeVideoPlayerFuture = _controller.initialize();

      // Use the controller to loop the video.
      _controller.setLooping(true);
    });
  }

  VideoPlayerController _controller;

  void requestPersmission() async {
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  }

  Future<void> _initializeVideoPlayerFuture;

  Future _handleClickFirst() async {
    if (_isClean) {

    } else {
      await _getImages();
    }
  }


  @override
  void initState() {
    requestPersmission();
    // Create and store the VideoPlayerController. The VideoPlayerController
    // offers several different constructors to play videos from assets, files,
    // or the internet.
    _controller = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );

    // Initialize the controller and store the Future for later use.
    _initializeVideoPlayerFuture = _controller.initialize();

    // Use the controller to loop the video.
    _controller.setLooping(false);

    super.initState();
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();

    super.dispose();
  }






  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return LoadingOverlay(
      isLoading: _saving,
      child: Scaffold(

        // Use a FutureBuilder to display a loading spinner while waiting for the
        // VideoPlayerController to finish initializing.
        body:

        FutureBuilder(
          future: _initializeVideoPlayerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Container(
                child: SingleChildScrollView(
                  child: Form(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                            height: 350,
                            width: MediaQuery
                                .of(context)
                                .size
                                .width,
                            // Use the VideoPlayer widget to display the video.
                            child: VideoPlayer(_controller)
                        ),
                        RoundedButton(
                          text: 'upload video',
                          press: (){_handleClickFirst();} ,
                        )
                      ],
                    ),
                  ),
                ),
                // If the VideoPlayerController has finished initialization, use
                // the data it provides to limit the aspect ratio of the video.
              );
            }
            else {
              // If the VideoPlayerController is still initializing, show a
              // loading spinner.
              return Center(child: CircularProgressIndicator());
            }
          },
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // Wrap the play or pause in a call to `setState`. This ensures the
            // correct icon is shown.
            setState(() {
              // If the video is playing, pause it.
              if (_controller.value.isPlaying) {
                _controller.pause();
                _controller.position.then((value) async {
                  _controller.seekTo(value);
                  var image = await ExportVideoFrame.exportImageBySeconds(
                      file, value, 0);

                  await ExportVideoFrame.saveImage(image, '');

                  ComplaintService.addComplaint(image).then((value) async {
                    DateTime rightNow = DateTime.now();
                    Complaint complaint = new Complaint(Emotion: value,
                        timestampsend: rightNow.toString(),
                        pathimg: image.path);
                    print(complaint.toJson());
                    Future<SharedPreferences> _prefs = SharedPreferences
                        .getInstance();
                    final SharedPreferences prefs = await _prefs;
                    print(jsonEncode(complaint.toJson()));
                    prefs.setString(complaint.timestampsend.toString(),
                        jsonEncode(complaint));
                    SweetAlert.show(context,
                        title: "Success",
                        subtitle: value.toString(),
                        confirmButtonColor: Colors.blue,
                        style: SweetAlertStyle.success);
                  });
                });
              } else {
                // If the video is paused, play it.
                _controller.play();
              }
            });
          },
          // Display the correct icon depending on the state of the player.
          child: Icon(
            _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
          ),

        ),


        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
