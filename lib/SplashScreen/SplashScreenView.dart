import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:emotiondetector/MainScreenList/wisdom_feed_page.dart';
import 'package:emotiondetector/addcomplaint/addcomplaint.dart';
import 'package:emotiondetector/addcomplaint/components/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';




class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreen> {

  bool _saving = true;
  @override
  Widget build(BuildContext context) {
    return initScreen(context);
  }


  @override
  void initState() {
    super.initState();
    getprefts();
    startTimer();
    Future.delayed(Duration(seconds: 5), () {

      // 5s over, navigate to a new page


    });
  }

  getprefts() async{
    final prefs = await SharedPreferences.getInstance();
    print("hi");
    print(prefs.getKeys());

     return json.decode(json.encode(prefs.get("1")));
  }
  startTimer() async {

    var duration = Duration(seconds: 5);
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => WisdomFeedPage()
    )
    );
  }

  initScreen(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Background(
        child : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: SvgPicture.asset(
                "assets/icons/emotion.svg",
                height: size.height * 0.35,
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            const SizedBox(height: 10.0),

            Text(
              "Welcome To",
              style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.white
              ),
            ),
            const SizedBox(height: 30.0),
            Text(
              "Emotions Detector",
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white
              ),
            ),
            const SizedBox(height: 50.0),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
              strokeWidth: 1,
            )
          ],
        ),
      ),
    );
  }


}