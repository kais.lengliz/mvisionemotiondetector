class Complaint {

  String Emotion;
  String timestampsend;
  String pathimg;

  Complaint._({this.Emotion, this.timestampsend, this.pathimg
  });

  Complaint({this.Emotion, this.timestampsend,this.pathimg});

  factory Complaint.fromJson(Map<String, dynamic> json) {
    return Complaint._(

      Emotion: json['Emotion'],
      timestampsend: json['timestampsend'],
      pathimg: json['pathimg'],
    );
  }

  Map<String, dynamic> toJson () =>
      {

        'Emotion': Emotion,
        'timestampsend': timestampsend,
        'pathimg' : pathimg,
      };
}