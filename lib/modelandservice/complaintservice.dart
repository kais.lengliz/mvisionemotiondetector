
import 'package:emotiondetector/modelandservice/url_link.dart';
import 'package:http/http.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'dart:convert';



class ComplaintService {
  static Future<String> addComplaint(  File _image) async
  {
    print("hiii");
    var stream = new ByteStream(DelegatingStream.typed(_image.openRead()));
    var length = await _image.length();
    String requestUrl = BackendUrl.url+"upload";
    var request = new MultipartRequest("POST", Uri.parse(requestUrl));


    var multipartFile = new MultipartFile('file', stream, length, filename: "file_avatar.jpg");
    request.files.add(multipartFile);
    var response = await request.send();
    return response.stream.bytesToString();
  }


}