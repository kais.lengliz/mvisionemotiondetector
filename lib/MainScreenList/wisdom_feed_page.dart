
import 'dart:convert';


import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:emotiondetector/addcomplaint/addcomplaint.dart';
import 'package:emotiondetector/landingpage.dart';
import 'package:emotiondetector/modelandservice/Complaint.dart';
import 'package:emotiondetector/modelandservice/complaintservice.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:loading_overlay/loading_overlay.dart';


class WisdomFeedPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WisdomFeedPageState();
}

class WisdomFeedPageState extends State<WisdomFeedPage> {
  var _saving = true;
  int currentindex = 0 ;
  List<Complaint> listComplaints = new List<Complaint>();







  @override
  void initState() {



  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [AddComplaint(), HistoriquePage()];
    List<String> titles = ["Check Emotion", "Emotions History"];
    return new WillPopScope(
      onWillPop: () async => false,
      child: new Scaffold(
          backgroundColor: Colors.white60,
          appBar: AppBar(
            title: Text(titles[currentindex]),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.blue,
            actions: <Widget>[

            ],
          ),
          bottomNavigationBar: CurvedNavigationBar(
            backgroundColor: Colors.cyan,
            items: <Widget>[
              Icon(Icons.emoji_emotions, size: 30 , color: Colors.blueAccent,),

              Icon(Icons.event_note, size: 30 , color: Colors.red,),
            ],
            onTap: (index) {
              print(index);
              print(widgets[index]);
              setState(() {
                currentindex = index ;

              });
              //Handle button tap
            },
          ),

          body: widgets[currentindex]
      ),
    );
  }


}


